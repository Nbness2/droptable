import nb.rspsUtil.dropTable.defaultKeys.Keys
import nb.rspsUtil.dropTable.buildMultiRollTable
import nb.rspsUtil.dropTable.util.increment
import nb.rspsUtil.dropTable.util.toProducer

private val multiRollTable = buildMultiRollTable<Player, Item> {
    name("Test MultiRoll Table")

    95.0 chance ItemProducer(995.toProducer())
      5.0 chance ItemProducer(4151.toProducer())
}

fun main() {
    val runs = 1_000_000

    val resultMap = mutableMapOf<List<Item>, Int>()

    val me = Keys.USE_TARGET(World.players[0])

    val rollsPerItem = Keys.ROLLS_PER_ITEM(1)

    repeat(runs) {
        val results = multiRollTable.produceRaw(rollsPerItem, me)
        resultMap.increment(results)
    }

    println(resultMap)
}
