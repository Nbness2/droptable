import kotlin.random.Random

fun String.randomString(randomStringLength: Int = Random.Default.nextInt(5, 15)): String {
    require(randomStringLength > 0)
    return buildString {
        repeat(randomStringLength) {
            append(this@randomString.random())
        }
    }
}

object World {
    val players = MutableList(5) {
        Player("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmonpqrstuvwxyz1234567890_".randomString())
    }

    fun announce(message: String) {
        players.forEach { it.sendString(message) }
    }
}

data class Player(val name: String) {
    fun sendString(message: String) {
        println("[MESSAGE TO PLAYER $name]: $message")
    }

    val world = World
}
