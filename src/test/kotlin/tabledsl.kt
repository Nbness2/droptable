import nb.rspsUtil.dropTable.defaultKeys.Keys
import nb.util.producerUtil.arguments.ArgumentKey
import nb.util.producerUtil.producer.ProducerResult
import nb.rspsUtil.dropTable.buildExclusiveTable
import nb.rspsUtil.dropTable.util.increment
import nb.rspsUtil.dropTable.util.toProducer
import kotlin.system.measureNanoTime

val bracketResults = ArgumentKey<MutableMap<String, Int>>("bracket_results")

val ammoTable = buildExclusiveTable<Player, Item> {
    name("Ammo")

    onSelect {
        //println("$mainName $subName $extendedSubName $argumentsUsed")
        argumentsUsed[bracketResults]!!.increment(subName)
    }

    table("Runes") {
        table("Elemental") {
            table("Common - Elemental") {
                weight(12)
                12.toProducer()
                addProducer(ItemProducer(12.toProducer()))
            }
            table("Common - Elemental") {
                weight(10)
                addProducer(ItemProducer(10.toProducer()))
            }
            table("Uncommon - Elemental") {
                weight(8)
                addProducer(ItemProducer(8.toProducer()))
            }
            table("Uncommon - Elemental") {
                weight(6)
                addProducer(ItemProducer(6.toProducer()))
            }
            table("Rare - Elemental") {
                weight(4)
                addProducer(ItemProducer(4.toProducer()))
            }
            table("Epic - Elemental") {
                weight(1)
                addProducer(ItemProducer(1.toProducer()))
            }
        }
        table("Catalytic") {
            table("Common - Catalytic") {
                weight(12)
                addProducer(ItemProducer(12.toProducer()))
            }
            table("Common - Catalytic") {
                weight(10)
                addProducer(ItemProducer(10.toProducer()))
            }
            table("Uncommon - Catalytic") {
                weight(8)
                addProducer(ItemProducer(8.toProducer()))
            }
            table("Uncommon - Catalytic") {
                weight(6)
                addProducer(ItemProducer(6.toProducer()))
            }
            table("Rare - Catalytic") {
                weight(4)
                addProducer(ItemProducer(4.toProducer()))
            }
            table("Epic - Catalytic") {
                weight(1)
                addProducer(ItemProducer(1.toProducer()))
            }
        }
    }
}


fun main() {
    val bracketResultMap = mutableMapOf<String, Int>()
    val results = mutableMapOf<Item, Int>()
    World
    val me = World.players[0]
    val tableToRoll = ammoTable
    val runs = 1_000_000
    var totalTimeNs = 0L
    repeat(runs) {
        var result: ProducerResult<Item>
        totalTimeNs += measureNanoTime {
            result = tableToRoll.produce(Keys.USE_TARGET(me), bracketResults(bracketResultMap))
        }
        val resultingValue = result.valueProduced
        results.increment(resultingValue)
    }

    println("Average time per produce: ${totalTimeNs / runs}ns")
    println(results)
    println(bracketResultMap)
}
