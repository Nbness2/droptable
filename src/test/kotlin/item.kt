import nb.util.producerUtil.arguments.Arguments
import nb.util.producerUtil.producer.Producer
import nb.util.producerUtil.producer.ProducerResult
import nb.util.producerUtil.producer.ProducerResultImpl
import nb.rspsUtil.dropTable.util.toProducer

data class Item(val itemId: Int, val itemAmount: Int) {
    override fun toString(): String = "(id:$itemId,amt:$itemAmount)"
}

class ItemProducer(val ids: Producer<Int>, val amounts: Producer<Int> = 1.toProducer()): Producer<Item> {
    override fun produce(arguments: Arguments): ProducerResult<Item> {
        val item = Item(ids.produceRaw(arguments), amounts.produceRaw(arguments))
        return ProducerResultImpl(item)
    }

    override fun toString(): String = "ItemProducer($ids, $amounts)"
}

class ItemProducerBuilder {
    private val idProducers: MutableList<Producer<Int>> = mutableListOf()
    private val amountProducers: MutableList<Producer<Int>> = mutableListOf()

    fun addIdProducer(producer: Producer<Int>): ItemProducerBuilder {
        idProducers.add(producer)
        return this
    }

    fun addIds(vararg ids: Int) = addIdProducer(ids.toProducer())

    fun addIds(producer: Producer<Int>) = addIdProducer(producer)

    fun addAmountProducer(producer: Producer<Int>): ItemProducerBuilder {
        amountProducers.add(producer)
        return this
    }

    fun addAmounts(vararg amounts: Int) = addAmountProducer(amounts.toProducer())

    fun addAmounts(producer: Producer<Int>) = addAmountProducer(producer)

    fun build(): ItemProducer = ItemProducer(idProducers.toProducer(), amountProducers.toProducer())
}

fun buildItemProducer(builder: ItemProducerBuilder.() -> Unit) = ItemProducerBuilder().apply(builder).build()
