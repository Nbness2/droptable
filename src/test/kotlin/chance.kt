
import nb.rspsUtil.dropTable.toChance
import nb.rspsUtil.dropTable.toPercent
import nb.rspsUtil.dropTable.util.increment

fun main() {
    val results = mutableMapOf<Boolean, Int>()
    val chanceValue = 25.0
    val flatModifier = 1.0
    val percentModifierValue = 25.0
    val chance = chanceValue.toChance()
    val percentModifier = percentModifierValue.toPercent()
    val runs = 1_000_000
    repeat(runs) {
        val result = chance.roll(flatModifier = flatModifier, percentModifier = percentModifier)
        results.increment(result)
    }
    val modifiedChance = (chanceValue + flatModifier) * (1 + (percentModifierValue / 100))
    val expectedResults = (runs * (modifiedChance / 100)).toInt()
    val actualResults = results[true]!!
    val delta = actualResults - expectedResults
    println("with modified chance: $modifiedChance%")
    println("expected: $expectedResults")
    println("actual: ${results[true]!!}")
    println("delta: $delta")
}
