package nb.rspsUtil.dropTable.util

/**
 * [ListRemap] is a way to remap a list in any order without creating a new version of the list.
 * Each index in [remapped] is an index that points to [original].
 *
 * @property [original] The original values that are being remapped
 * @property [remapped] The contained int in index X is index Y in original
 * @property [size] The size of the [ListRemap]
 *
 * @sample ListRemap(listOF("A", "B"), intArrayOf(0, 1, 1, 0, 0, 1)).toString() -> [A, B, B, A, A, B]
 * @constructor Creates a [ListRemap] with the given [remapped] indices to point to [original]
 */
internal class ListRemap<T>(val original: List<T>, remapped: IntArray): List<T> {

    private class IndexNotValid(override val message: String) : Exception()

    init {
        for (index in remapped) {
            if (index !in original.indices) {
                throw IndexNotValid("Index $index not valid. Must be in ${original.indices}")
            }
        }
    }

    val remapped = remapped.copyOf()
    override val size: Int = remapped.size

    /**
     * Get the item at [original] [[remapped] [[index]]]
     *
     * @param [index] The index to get from [remapped]
     * @return [T]
     */
    override operator fun get(index: Int): T =  original[remapped[index]]
    override fun toString(): String = this.map { it }.toString()
    override fun isEmpty(): Boolean = remapped.isEmpty()
    override fun contains(element: T): Boolean {
        if (isEmpty()) return false
        for (thing in this) {
            if (element == thing) {
                return true
            }
        }
        return false
    }
    override fun containsAll(elements: Collection<T>): Boolean {
        if (isEmpty()) return false
        for (element in elements) {
            if (element !in this) {
                return false
            }
        }
        return true
    }
    override fun indexOf(element: T): Int {
        if (isEmpty()) return -1
        for ((currentIndex, thing) in this.withIndex()) {
            if (element == thing)
                return currentIndex
        }
        return -1
    }
    override fun lastIndexOf(element: T): Int {
        if (isEmpty()) return -1
        var lastIndex = -1
        for ((currentIndex, thing) in this.withIndex()) {
            if (element == thing) {
                lastIndex = currentIndex
            }
        }
        return lastIndex
    }
    override fun iterator(): Iterator<T> = BasicItr(this)
    override fun listIterator(): ListIterator<T> = BasicListItr(this)
    override fun listIterator(index: Int): ListIterator<T> = BasicListItr(this, index)
    override fun subList(fromIndex: Int, toIndex: Int): List<T> {
        if (fromIndex !in this.indices)
            throw IndexOutOfBoundsException("fromIndex ($fromIndex) not in range ${this.indices}")
        if (toIndex !in this.indices)
            throw IndexOutOfBoundsException("toIndex ($toIndex) not in range ${this.indices}")
        val subList = mutableListOf<T>()
        for (index in fromIndex until toIndex)
            subList.add(this[index])
        return subList
    }
}

/**
 * Basic iterator that iterates over [dataToIterate]
 * @constructor Creates an [Iterator] of [dataToIterate]
 */
private class BasicItr<T>(private val dataToIterate: List<T>): Iterator<T> {
    var currentIndex: Int = 0
    override fun hasNext(): Boolean = currentIndex+1 <= dataToIterate.size
    override fun next(): T = dataToIterate[currentIndex++]
}

/**
 * Basic [ListIterator] implementation that iterates over [dataToIterate]
 * @constructor Creates a [ListIterator] that starts at index [currentIndex] that iterates over [dataToIterate]
 */
private class BasicListItr<T>(private val dataToIterate: List<T>, var currentIndex: Int = 0): ListIterator<T> {
    override fun hasNext(): Boolean = currentIndex + 1 <= dataToIterate.size
    override fun nextIndex(): Int = currentIndex + 1
    override fun next(): T {
        if (currentIndex == -1) ++currentIndex
        return dataToIterate[currentIndex++]
    }

    override fun hasPrevious(): Boolean = currentIndex - 1 >= -1
    override fun previousIndex(): Int = currentIndex - 1
    override fun previous(): T {
        if (currentIndex == dataToIterate.size) --currentIndex
        return dataToIterate[currentIndex--]
    }
}
