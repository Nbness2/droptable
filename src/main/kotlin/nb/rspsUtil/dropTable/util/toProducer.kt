package nb.rspsUtil.dropTable.util

import nb.rspsUtil.dropTable.random.randomTableOf
import nb.util.producerUtil.arguments.Arguments
import nb.util.producerUtil.producer.Producer
import nb.util.producerUtil.producer.ProducerResultImpl

class EmptyProducer<T: Any>: Producer<T> {
    override fun produce(arguments: Arguments) = throw Exception("Empty Producer")
    override fun toString(): String = "EmptyProducer"
}

class SingletonProducer<T: Any>(private val value: T): Producer<T> {
    override fun produce(arguments: Arguments) = ProducerResultImpl(value)
    override fun toString(): String = "SingletonProducer($value)"
}

class CollectionProducer<T: Any>(val producers: Collection<Producer<T>>): Producer<T>, Collection<Producer<T>> by producers {
    override fun produce(arguments: Arguments) = ProducerResultImpl(producers.random().produceRaw(arguments))
    override fun toString(): String = "ListProducer$producers"
}

@JvmInline
value class ByteArrayProducer(val byteArray: ByteArray): Producer<Byte> {
    override fun produce(arguments: Arguments) = ProducerResultImpl(byteArray.random())
    override fun toString(): String = "ByteArrayProducer${byteArray.contentToString()}"
}

@JvmInline
value class ShortArrayProducer(val shortArray: ShortArray): Producer<Short> {
    override fun produce(arguments: Arguments) = ProducerResultImpl(shortArray.random())
    override fun toString(): String = "IntArrayProducer${shortArray.contentToString()}"
}

@JvmInline
value class IntArrayProducer(val intArray: IntArray): Producer<Int> {
    override fun produce(arguments: Arguments) = ProducerResultImpl(intArray.random())
    override fun toString(): String = "IntArrayProducer${intArray.contentToString()}"
}

@JvmInline
value class LongArrayProducer(val longArray: LongArray): Producer<Long> {
    override fun produce(arguments: Arguments) = ProducerResultImpl(longArray.random())
    override fun toString(): String = "LongArrayProducer${longArray.contentToString()}"
}

@JvmInline
value class FloatArrayProducer(val floatArray: FloatArray): Producer<Float> {
    override fun produce(arguments: Arguments) = ProducerResultImpl(floatArray.random())
    override fun toString(): String = "FloatArrayProducer${floatArray.contentToString()}"
}

@JvmInline
value class DoubleArrayProducer(val doubleArray: DoubleArray): Producer<Double> {
    override fun produce(arguments: Arguments) = ProducerResultImpl(doubleArray.random())
    override fun toString(): String = "FloatArrayProducer${doubleArray.contentToString()}"
}

fun <T: Any> T.toProducer(): Producer<T> = SingletonProducer(this)

fun <T: Any> Collection<Producer<T>>.toProducer(): Producer<T> {

    if (this.isEmpty()) {
        return EmptyProducer()
    }

    val retval =  if (this is Producer<*>) {
        when (this.size) {
            1 -> this.first()
            else -> this
        }
    } else {
        when (this.size) {
            1 -> this.first()
            else -> CollectionProducer(this)
        }
    } as Producer<T>

    return retval
}

@JvmName("listToProducer")
fun <T: Any> List<Producer<T>>.toProducer(): Producer<T> {

    if (this.isEmpty()) {
        return EmptyProducer()
    }

    val retval =  if (this is Producer<*>) {
        when (this.size) {
            1 -> this[0]
            else -> this
        }
    } else {
        when (this.size) {
            1 -> this[0]
            else -> CollectionProducer(this)
        }
    } as Producer<T>

    return retval
}

@JvmName("mutableListToProducer")
fun <T: Any> MutableList<Producer<T>>.toProducer(): Producer<T> = (this as List<Producer<T>>).toProducer()

inline fun <reified T: Any> Array<T>.toProducer(): Producer<T> = when(T::class) {
    Byte::class -> (this as Array<Byte>).toByteArray().toProducer()
    Short::class -> (this as Array<Short>).toShortArray().toProducer()
    Int::class -> (this as Array<Int>).toIntArray().toProducer()
    Long::class -> (this as Array<Long>).toLongArray().toProducer()
    Float::class -> (this as Array<Float>).toFloatArray().toProducer()
    Double::class -> (this as Array<Double>).toDoubleArray().toProducer()
    Producer::class -> CollectionProducer((this.toList() as List<Producer<Any>>))
    else -> randomTableOf(*this)
} as Producer<T>

fun ByteArray.toProducer(): Producer<Byte> = ByteArrayProducer(this)
fun ShortArray.toProducer(): Producer<Short> = ShortArrayProducer(this)
fun IntArray.toProducer(): Producer<Int> = IntArrayProducer(this)
fun LongArray.toProducer(): Producer<Long> = LongArrayProducer(this)
fun FloatArray.toProducer(): Producer<Float> = FloatArrayProducer(this)
fun DoubleArray.toProducer(): Producer<Double> = DoubleArrayProducer(this)

