package nb.rspsUtil.dropTable

import nb.rspsUtil.dropTable.defaultKeys.Keys
import nb.rspsUtil.dropTable.exception.NoTargetFound
import nb.rspsUtil.dropTable.random.MemoizedWeightedTable
import nb.util.producerUtil.arguments.Arguments
import nb.util.producerUtil.producer.*
import nb.rspsUtil.dropTable.util.toProducer
import kotlin.random.Random

open class ExclusiveTable<T: Any, R: Any>(
    override val tableName: String,
    weights: IntArray,
    producers: List<Producer<R>>,
    random: Random = Random.Default,
    override inline val onSelect: OnSelectContext<T, R>.() -> Unit = { }
): Table<T, R> {


    companion object {
        val NO_TABLE = ExclusiveTable<Any, Any>("NO_TABLE_PROVIDED", intArrayOf(), emptyList())
    }

    protected open val table = MemoizedWeightedTable(
        items = producers,
        weights = weights,
        random = random
    )

    override fun produce(arguments: Arguments): ProducerResult<R> {

        arguments.putIfAbsent(Keys.MAIN_NAME, this.tableName)

        arguments.putIfAbsent(Keys.USE_TABLE, this)

        arguments[Keys.SUB_NAME] = this.tableName

        val exSubName = if (arguments[Keys.MAIN_NAME] == this.tableName) {
            this.tableName
        } else {
            val exSub = arguments.getOrDefault(Keys.EXTENDED_SUB_NAME, "")
            "$exSub>$tableName"
        }

        arguments[Keys.EXTENDED_SUB_NAME] = exSubName

        val target = arguments[Keys.USE_TARGET<T>()] ?: throw NoTargetFound(this)

        val producedBracketResult = this.table.produce(arguments = arguments)

        val producedProducer = producedBracketResult.valueProduced

        val producedValueResult = producedProducer.produce(arguments)

        val valueProduced = producedValueResult.valueProduced

        val selectContext = OnSelectContext(
            target = target,
            mainName = this.tableName,
            subName = arguments.getOrDefault(Keys.SUB_NAME, this.tableName),
            extendedSubName = exSubName,
            valueProduced = valueProduced,
            argumentsUsed = arguments
        )

        selectContext.onSelect()

        return ProducerResultImpl(producedValueResult.valueProduced, producedValueResult.argumentsUsed)
    }

    override fun toString(): String = "ExclusiveTable($tableName)"

    open class Builder<T: Any, R: Any>(
        tableName: String = "NO_TABLE_NAME",
        @TableDSL inline var onSelect: OnSelectContext<T, R>.() -> Unit = { },

        ) {
        @TableDSL
        class BuilderWrapper<T: Any, R: Any>(
            tableName: String,
            onSelect: OnSelectContext<T, R>.() -> Unit = { }
        ): Builder<T, R>(tableName, onSelect) {
            @TableDSL
            var weight: Int = 0

            @TableDSL
            fun weight(newWeight: Int) {
                this.weight = newWeight
            }

            @TableDSL
            fun buildWeighted() = this.weight to super.build()
        }

        @TableDSL
        var random: Random = Random.Default

        @TableDSL
        fun random(newRandom: Random): Builder<T, R> {
            this.random = newRandom
            return this
        }

        @TableDSL
        var name: String = tableName

        @TableDSL
        fun name(newName: String): Builder<T, R> {
            this.name = newName
            return this
        }

        @TableDSL
        private val weightToProducer: MutableMap<Int, MutableList<Producer<R>>> = mutableMapOf()

        @TableDSL
        fun addProducer(producer: Producer<R>, weight: Int = 0): Builder<T, R> {
            weightToProducer.putIfAbsent(weight, mutableListOf())
            weightToProducer[weight]!!.add(producer)
            return this
        }

        @TableDSL
        fun table(
            tableName: String = "NO_TABLE_NAME",
            inherit: Boolean = false,
            block: BuilderWrapper<T, R>.() -> Unit
        ): Builder<T, R> {
            val builder: BuilderWrapper<T, R> = BuilderWrapper(tableName)
            if (inherit) {
                builder.onSelect(this.onSelect)
            }
            val (weight: Int, table: Producer<R>) = builder.apply(block).buildWeighted()

            return addProducer(table, weight)
        }

        @TableDSL
        fun onSelect(block: OnSelectContext<T, R>.() -> Unit): Builder<T, R> {
            this.onSelect = block
            return this
        }

        fun build(): Table<T, R> {
            if (weightToProducer.isEmpty()) {
                return NO_TABLE as Table<T, R>
            }
            var max = weightToProducer.keys.maxOrNull()!!
            if (max == 0) {
                max = 1
            }
            weightToProducer.putIfAbsent(max, mutableListOf())
            if (weightToProducer[0] != null) {
                weightToProducer[max]!!.addAll(weightToProducer[0]!!)
                weightToProducer.remove(0)
            }
            val sorted = weightToProducer.toSortedMap()
            val weightArray = sorted.keys.toIntArray()
            val producers = sorted.values.map { it.toProducer() }

            val table = ExclusiveTable(
                tableName = name,
                weights = weightArray,
                producers = producers,
                random = this.random,
                onSelect = onSelect
            )
            return table
        }
    }
}
