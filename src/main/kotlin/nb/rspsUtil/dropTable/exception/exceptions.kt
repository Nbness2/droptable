package nb.rspsUtil.dropTable.exception

import nb.rspsUtil.dropTable.Table

class NoTargetFound(val table: Table<*, *>): Exception("No target was found when rolling on table named: ${table.tableName}")
