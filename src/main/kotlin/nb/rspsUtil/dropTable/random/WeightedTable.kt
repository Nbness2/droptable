package nb.rspsUtil.dropTable.random

import nb.util.producerUtil.arguments.Arguments
import nb.rspsUtil.dropTable.random.range.RandomLongRange
import nb.rspsUtil.dropTable.random.WeightStatus.*
import nb.rspsUtil.dropTable.util.increment
import nb.rspsUtil.dropTable.util.modifyLong
import nb.rspsUtil.dropTable.util.sortedByOtherArray
import kotlin.random.Random

/**
 * @author Curtis Woodard <nbness2@gmail.com>
 *
 * A [RandomTable] that is also weighted.
 * [values] and [weights] must be the same length.
 * [weights] cannot contain more than 1 of the same weight.
 * The item at index X will correspond to the weight at index X (the items are sorted accordingly with the weights that are sorted in ascending order)
 *
 * @constructor Creates a new [WeightedTable]
 * @property [values] The items that will be picked from
 * @property [weights] The weights sorted in ascending order
 * @param [seed] The seed to initialize the [WeightedTable] with
 * @property [weightModifier] The formula used to calculate the weight with a given modifier
 * @property [weightRange] The range of weights that can be picked
 */
open class WeightedTable<T>(
        items: List<T>,
        weights: IntArray,
        random: Random = Random.Default
): RandomTable<T>(items.sortedByOtherArray(weights), random) {

        protected val weights: IntArray = weights.sortedArray()

        val weightValues = this.weights.copyOf()

        val weightRange: RandomLongRange

        /**
         * Assesses the status of the weights [IntArray] against the items [other].
         * This is to check that they are the same size, there are no duplicate weights and there are no negative weights.
         *
         * @param [other] The items to check the weights against
         * @return [WeightStatus]
         */
        protected fun IntArray.assessWeightsAgainst(other: List<T>): WeightStatus {
                if (this === weights) {
                        return WeightStatus.ValidWeights
                }
                if (this.size != other.size) {
                        return InvalidLength
                }
                for ((index, weight) in this.withIndex()) {
                        if (this.count { it == weight } > 1) {
                                return WeightStatus.DuplicateWeight(indexOfFirst { it == weight }, indexOfLast { it == weight } )
                        } else if (weight < 1) {
                                return WeightStatus.InvalidWeight(index)
                        }
                }
                return WeightStatus.ValidWeights
        }

        /**
         * Gets the index of the first [item] in [values]
         *
         * @param [item] The item to get the index of
         * @return [Int]
         */
        fun indexOf(item: T): Int = this.values.indexOfFirst { it == item }

        /**
         * Gets the default weight of the first [item] in [values]
         *
         * @param [item] The item to get the weight of
         * @return [Int]
         */
        fun weightOf(item: T): Int = this.weights[indexOf(item)]

        /**
         * Gets the default (no custom weights) chance for [item] to be picked (or 0.0 if [item] is not contained)
         *
         * @oaram[item] The item to get the chance of
         * @return [Double]
         */
        fun chanceOf(item: T): Double = if (item in this) {
                (this.weights[this.indexOf(item)].toDouble() * 100) / this.weightRange.maximum
        } else {
                0.0
        }

        /**Supplementary functions for extensibility*/
        protected open fun checkOtherWeights(otherWeights: IntArray): Boolean {
                otherWeights.assessWeightsAgainst(this.values).doWhenInvalid { return false }
                return true
        }

        /**Supplementary functions for extensibility*/
        protected open fun getWeightRangeFor(otherWeights: IntArray): RandomLongRange = RandomLongRange(1L, otherWeights.sumOf(Int::toLong), true)

        /**Supplementary functions for extensibility*/
        protected open fun getItemsFor(otherWeights: IntArray): List<T> = if (otherWeights === this.weights) {
                this.values
        } else {
                this.values.sortedByOtherArray(otherWeights, true)
        }

        /**Supplementary functions for extensibility*/
        protected open fun getWeightsFor(otherWeights: IntArray): IntArray = otherWeights.copyOf()

        protected fun processArguments(arguments: Arguments): Arguments {
                val useWeights = arguments[USE_WEIGHTS, this.weights]
                if (useWeights.contentEquals(this.weights) || !checkOtherWeights(useWeights)) {
                        return arguments.copyOf(
                                USE_ITEMS<T>()(this.values),
                                USE_WEIGHTS(this.weights),
                                USE_WEIGHT_RANGE(this.weightRange)
                        )
                }

                return arguments.copyOf(
                        USE_ITEMS<T>()(getItemsFor(useWeights)),
                        USE_WEIGHTS(getWeightsFor(useWeights)),
                        USE_WEIGHT_RANGE(getWeightRangeFor(useWeights))
                )
        }

        override fun nextValue(arguments: Arguments): T {
                val processedArguments = processArguments(arguments)

                return verifiedNextValue(
                        flatModifier = processedArguments[FLAT_WEIGHT_MODIFIER, 0.0],
                        percentModifier = processedArguments[PERCENT_WEIGHT_MODIFIER, 0.0],
                        customWeights = processedArguments[USE_WEIGHTS, this.weights],
                        customItems = processedArguments[USE_ITEMS(), this.values],
                        weightRange = processedArguments[USE_WEIGHT_RANGE, this.weightRange]
                )
        }


        /**
         * Protected function that is a micro optimization
         *
         * @param [nb.util.producerUtil.arguments] The nb.util.producerUtil.arguments to use to generate a next value
         * @return [T]
         */
        protected fun verifiedNextValue(
                flatModifier: Double,
                percentModifier: Double,
                customWeights: IntArray,
                customItems: List<T>,
                weightRange: RandomLongRange
        ): T {
                val pickedWeight = weightRange.nextValue()
                var weight = modifyLong(pickedWeight, flatModifier, percentModifier)

                for ((weightValue, item) in customWeights zip customItems) {
                        weight -= weightValue
                        if (weight <= 0) {
                                return item
                        }
                }

                return customItems.last()
        }

        /**
         * Returns a [Map] of size [PRODUCE_AMOUNT] (or 1) full of [verifiedNextValue]
         * @param [arguments] the [Arguments] to pas in to [verifiedNextValue]
         * @return [Map]<[T], [Int]>
         */
        override fun nextValueMap(arguments: Arguments): Map<T, Int> {
                val processedArguments = processArguments(arguments)
                val map = HashMap<T, Int>()
                val amountToProduce = arguments[PRODUCE_AMOUNT, 1]

                repeat(amountToProduce) {
                        map.increment(
                                verifiedNextValue(
                                        flatModifier = processedArguments[FLAT_WEIGHT_MODIFIER, 0.0],
                                        percentModifier = processedArguments[PERCENT_WEIGHT_MODIFIER, 0.0],
                                        customWeights = processedArguments[USE_WEIGHTS, this.weights],
                                        customItems = processedArguments[USE_ITEMS(), this.values],
                                        weightRange = processedArguments[USE_WEIGHT_RANGE, this.weightRange]
                                )
                        )
                }

                return map
        }

        /**
         * Returns a [List] of size [PRODUCE_AMOUNT] (or 1) full of [verifiedNextValue].
         *
         * @param [arguments] The nb.util.producerUtil.arguments to pass to [verifiedNextValue]
         * @return [List]<[T]>
         */
        override fun nextValueList(arguments: Arguments): List<T> {
                val processedArguments = processArguments(arguments)
                val amountToProduce = arguments[PRODUCE_AMOUNT, 1]

                return List(amountToProduce) {
                        verifiedNextValue(
                                flatModifier = processedArguments[FLAT_WEIGHT_MODIFIER, 0.0],
                                percentModifier = processedArguments[PERCENT_WEIGHT_MODIFIER, 0.0],
                                customWeights = processedArguments[USE_WEIGHTS, this.weights],
                                customItems = processedArguments[USE_ITEMS(), this.values],
                                weightRange = processedArguments[USE_WEIGHT_RANGE, this.weightRange]
                        )
                }
        }


        init {
                weights.assessWeightsAgainst(this.values).throwWhenBad()
                this.weightRange = RandomLongRange(1L, weights.sumOf(Int::toLong), true, this.internalRandom)
        }

}
