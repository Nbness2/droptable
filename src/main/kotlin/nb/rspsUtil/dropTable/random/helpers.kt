package nb.rspsUtil.dropTable.random

import nb.rspsUtil.dropTable.random.range.RandomLongRange

fun <T> randomTableOf(vararg items: T) = RandomTable(items = items.toList())

fun <T> weightedRandomTableOf(vararg items: Pair<Int, T>): WeightedTable<T> {
    val tableWeights = items.map { it.first }.toIntArray()
    val tableItems = items.map { it.second }
    return WeightedTable<T>(items = tableItems, weights = tableWeights)
}

data class MemoizedValues<T>(
    val range: RandomLongRange,
    val valueList: List<T>,
    val weights: IntArray
)
