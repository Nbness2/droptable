package nb.rspsUtil.dropTable.random

import nb.rspsUtil.dropTable.util.increment
import nb.util.producerUtil.arguments.ArgumentKey
import nb.util.producerUtil.arguments.Arguments
import nb.util.producerUtil.producer.Producer
import nb.util.producerUtil.producer.ProducerResult
import nb.util.producerUtil.producer.ProducerResultImpl
import kotlin.random.Random

/** @author: Curtis Woodard <nbness2@gmail.com> */

/**
 * None of the protected functions are meant to be exposed for the purpose of possibly nb.util.producerUtil.random generators or nb.util.producerUtil.random
 * collections. Why would something that generates a List have a nextFloat or nextByte function?
 * All functions inside are just [Random.nextInt] or [Random.nextDouble] with the result converted to the return type.
 */
abstract class BaseRandom<T> (random: Random): Producer<T> {

    private class InclusiveOverflow(override val message: String): Exception()

    protected var internalRandom: Random = random

    /**
     * Generates a [Byte] within the range [lowerBound] and [upperBound] with [inclusive] defining inclusivity
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value (depending on [inclusive]) to generate
     * @param [inclusive] Whether or not to include the actual [upperBound]
     * @return [Byte]
     */
    protected fun nextByte(lowerBound: Byte, upperBound: Byte, inclusive: Boolean): Byte =
        internalRandom.nextInt(lowerBound.toInt(), (upperBound + (if (inclusive) 1 else 0))).toByte()

    /**
     * Generates a [Short] within the range [lowerBound] and [upperBound] with [inclusive] defining inclusivity
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value (depending on [inclusive]) to generate
     * @param [inclusive] Whether or not to include the actual [upperBound]
     * @return [Short]
     */
    protected fun nextShort(lowerBound: Short, upperBound: Short, inclusive: Boolean): Short =
        internalRandom.nextInt(lowerBound.toInt(), (upperBound + (if (inclusive) 1 else 0))).toShort()

    /**
     * Generates an [Int] within the range [lowerBound] and [upperBound] with [inclusive] defining inclusivity
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value (depending on [inclusive]) to generate
     * @param [inclusive] Whether or not to include the actual [upperBound]
     * @return [Int]
     */
    protected fun nextInt(lowerBound: Int, upperBound: Int, inclusive: Boolean): Int =
        internalRandom.nextLong(lowerBound.toLong(), upperBound + (if (inclusive) 1 else 0).toLong()).toInt()

    /**
     * Generates a [Long] within the range [lowerBound] and [upperBound] with [inclusive] defining inclusivity
     * Can only be [inclusive] if the delta between [lowerBound] and [upperBound] is less than [ULong.MAX_VALUE] due to overflow problems
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value (depending on [inclusive]) to generate
     * @param [inclusive] Whether or not to include the actual [upperBound]
     * @return [Long]
     */
    protected fun nextLong(lowerBound: Long, upperBound: Long, inclusive: Boolean = false): Long {
        if (inclusive) {
            if (upperBound == Long.MAX_VALUE && lowerBound == Long.MIN_VALUE) {
                throw InclusiveOverflow("Cannot be inclusive when range is ${Long.MIN_VALUE .. Long.MAX_VALUE} due to overflow")
            }
            if (upperBound == Long.MAX_VALUE) {
                return internalRandom.nextLong(lowerBound-1, upperBound) + 1
            }
            return internalRandom.nextLong(lowerBound, upperBound + 1)
        }
        return internalRandom.nextLong(lowerBound, upperBound)
    }

    /**
     * Generates a [Float] within the range [lowerBound] and [upperBound]
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value
     * @return [Float]
     */
    protected fun nextFloat(lowerBound: Float, upperBound: Float): Float =
        internalRandom.nextDouble(lowerBound.toDouble(), upperBound.toDouble()).toFloat()

    /**
     * Generates a [Double] within the range [lowerBound] and [upperBound]
     *
     * @param [lowerBound] The minimum value to generate
     * @param [upperBound] The maximum value
     * @return [Double]
     */
    protected fun nextDouble(lowerBound: Double, upperBound: Double): Double =
        internalRandom.nextDouble(lowerBound, upperBound)

    /**
     * Generates a new value of type [T] potentially using nb.util.producerUtil.arguments in [arguments]
     *
     * @return [T]
     */
    abstract fun nextValue(arguments: Arguments): T

    /**
     * An easier way to call [nextValue] for the end user on the fly
     *
     * @return [T]
     */
    fun nextValue(vararg arguments: Pair<ArgumentKey<*>, Any?>) = nextValue(Arguments(arguments = arguments))

    /**
     * Generates a [Map]<[T], [Int]> by calling [nextValue] [amount] times with the given modifier [modifier]
     *
     * @param[amount] The amount of times to call [nextValue]
     * @param[modifier] The modifier to pass in to [nextValue]
     * @return [Map]<[T], [Int]>
     */
    open fun nextValueMap(arguments: Arguments): Map<T, Int> {
        val valueMap = mutableMapOf<T, Int>()
        val valuesToProduce = arguments[PRODUCE_AMOUNT]!!
        repeat(valuesToProduce) {
            valueMap.increment(nextValue(arguments = arguments))
        }
        return valueMap
    }

    fun nextValueMap(vararg arguments: Pair<ArgumentKey<*>, Any?>) = nextValueMap(Arguments(arguments = arguments))

    /**
     * Generates a [List]<[T]> of size [amount] by calling [nextValue]
     *
     * @param[amount] The size of the [List]
     * @param[modifier] The modifier to pass in to [nextValue]
     * @return [List]<[T]>
     */
    open fun nextValueList(arguments: Arguments): List<T> {
        val valuesToProduce = arguments[PRODUCE_AMOUNT]!!
        return List(valuesToProduce) {
            nextValue(arguments)
        }
    }

    fun nextValueList(vararg arguments: Pair<ArgumentKey<*>, Any?>): List<T> = nextValueList(Arguments(arguments = arguments))


    /**
     * Creates a copy of this [BaseRandom]
     *
     * @return [BaseRandom]
     */
    abstract fun copyOf(): BaseRandom<T>

    fun replaceRandom(withRandom: Random) { internalRandom = withRandom }

    override fun produce(arguments: Arguments): ProducerResult<T> = ProducerResultImpl(nextValue(arguments = arguments))
}
