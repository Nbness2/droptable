package nb.rspsUtil.dropTable.random

import nb.rspsUtil.dropTable.random.range.RandomLongRange
import nb.rspsUtil.dropTable.util.sortedListRemap
import kotlin.random.Random

/**
 * [MemoizedWeightedTable] is a memory-speed tradeoff compared to [WeightedTable] and is optimized for frequent use of other weights.
 * Weights that pass [assessWeightsAgainst] will have its [hashCode] assigned to a [Triple] consisting of the weight range, the [ListRemap]<[T]> of the sorted items and an [IntArray] of the sorted weights.
 * Weights that fail [assessWeightsAgainst] will have its [hashCode] assigned to a null.
 * Calling [getItemsFor], [getWeightsFor] and [getWeightRangeFor] on weights that failed [assessWeightsAgainst] will return [values], [weights] and [weightRange]
 *
 * @property [memoized] A [HashMap]<[Int], [Triple]<[RandomLongRange], [List]<[T]>, [IntArray]>?> that caches the results of any custom weights put in to it.
 * @constructor Creates a [MemoizedWeightedTable]
 */
open class MemoizedWeightedTable<T>(
    items: List<T>,
    weights: IntArray,
    random: Random = Random.Default
): WeightedTable<T>(items = items, weights = weights, random = random) {

    protected val memoized: HashMap<IntArray, MemoizedValues<T>?> = hashMapOf()

    protected fun memoizeWeights(weightsToMemoize: IntArray) {
        if (weightsToMemoize !in memoized) {
            weightsToMemoize.assessWeightsAgainst(values)
                .doWhenInvalid { memoized[weightsToMemoize] = null }
                .doWhenValid {
                    memoized[weightsToMemoize] = MemoizedValues(
                            range = RandomLongRange(1L, weightsToMemoize.sumOf(Int::toLong), true),
                            valueList = sortedListRemap(values, weightsToMemoize),
                            weights = weightsToMemoize.sortedArray()
                        )
                }
        }
    }

    override fun getWeightRangeFor(otherWeights: IntArray): RandomLongRange = if (memoized[otherWeights] == null) {
        this.weightRange
    } else {
        this.memoized[otherWeights]!!.range
    }

    override fun getItemsFor(otherWeights: IntArray): List<T> = if (memoized[otherWeights] == null) {
        this.values
    } else {
        this.memoized[otherWeights]!!.valueList
    }

    override fun getWeightsFor(otherWeights: IntArray): IntArray = if (memoized[otherWeights] == null) {
        this.weights
    } else {
        this.memoized[otherWeights]!!.weights
    }

    override fun checkOtherWeights(otherWeights: IntArray): Boolean {
        memoizeWeights(otherWeights)
        return this.memoized[otherWeights] == null
    }

    init {
        memoizeWeights(weights)
    }
}
