package nb.rspsUtil.dropTable.random.progression

import nb.util.producerUtil.arguments.Arguments
import nb.rspsUtil.dropTable.random.BaseRandom
import nb.rspsUtil.dropTable.random.range.RandomByteRange
import kotlin.math.absoluteValue
import kotlin.math.floor
import kotlin.random.Random

class RandomByteProgression(
    first: Byte,
    last: Byte,
    step: Int = 1,
    random: Random = Random.Default
): BaseRandom<Byte>(random) {
    val first: Byte
    val last: Byte
    val step: Int
    private val base: Byte
    private val scalar: RandomByteRange

    init {
        require(step > 0) {
            throw InvalidStep("Invalid step [$step]: must be greater than 0")
        }
        this.base = first
        if (first < last) {
            this.first = first
            this.last = (first .. last step step).last.toByte()
            this.step = step
            this.scalar = RandomByteRange(
                minimum = 0,
                maximum = (floor(((last - first) + step).toDouble()) / step).toInt().toByte(),
                inclusive = false
            )
        } else {
            this.first = last
            this.last = (last .. first step step).last.toByte()
            this.step = (-step)
            this.scalar = RandomByteRange(
                minimum = 0,
                maximum = floor(((first - last) - step).toDouble() / step).absoluteValue.toInt().toByte(),
                inclusive = false
            )
        }
    }

    override fun copyOf(): RandomByteProgression = RandomByteProgression(
        first = this.first,
        last = this.last,
        step = this.step,
        random = this.internalRandom
    )

    override fun nextValue(arguments: Arguments): Byte = (this.base + (this.scalar.nextValue() * this.step)).toByte()
}
