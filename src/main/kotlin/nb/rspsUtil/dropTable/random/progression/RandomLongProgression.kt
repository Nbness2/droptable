package nb.rspsUtil.dropTable.random.progression

import nb.util.producerUtil.arguments.Arguments
import nb.rspsUtil.dropTable.random.BaseRandom
import nb.rspsUtil.dropTable.random.range.RandomLongRange
import kotlin.math.absoluteValue
import kotlin.math.floor
import kotlin.random.Random

class RandomLongProgression(
    first: Long,
    last: Long,
    step: Long = 1,
    random: Random = Random.Default
): BaseRandom<Long>(random) {
    val first: Long
    val last: Long
    val step: Long
    private val base: Long
    private val scalar: RandomLongRange

    init {
        require(step > 0) {
            throw InvalidStep("Invalid step [$step]: must be greater than 0")
        }
        this.base = first
        if (first < last) {
            this.first = first
            this.last = (first .. last step step).last
            this.step = step
            this.scalar = RandomLongRange(
                minimum = 0,
                maximum = (floor(((last - first) + step).toDouble()) / step).toLong(),
                inclusive = false
            )
        } else {
            this.first = last
            this.last = (last .. first step step).last
            this.step = (-step)
            this.scalar = RandomLongRange(
                minimum = 0,
                maximum = floor(((first - last) - step).toDouble() / step).absoluteValue.toLong(),
                inclusive = false
            )
        }
    }

    override fun copyOf(): RandomLongProgression = RandomLongProgression(
        first = this.first,
        last = this.last,
        step = this.step,
        random = this.internalRandom
    )

    override fun nextValue(arguments: Arguments): Long = (this.base + (this.scalar.nextValue() * this.step))
}
