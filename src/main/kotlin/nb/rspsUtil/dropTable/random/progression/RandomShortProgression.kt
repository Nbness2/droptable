package nb.rspsUtil.dropTable.random.progression

import nb.util.producerUtil.arguments.Arguments
import nb.rspsUtil.dropTable.random.BaseRandom
import nb.rspsUtil.dropTable.random.range.RandomShortRange
import kotlin.math.absoluteValue
import kotlin.math.floor
import kotlin.random.Random


/**
 * Generates a [Short] in uniform from [first] to [last] with a gap of [step] between each generated value.
 *
 * @property [first] The first value in the [RandomShortProgression]
 * @property [last] The last value in the [RandomShortProgression]
 * @property [step] The delta between a given value and the possible next value in the progression
 * @property [base] The base value (also the first value)
 * @property [scalar] The scalar range that determines actual [step]
 * @constructor Creates a new [RandomShortProgression] from [first] [last] and [step].
 */
class RandomShortProgression(
    first: Short,
    last: Short,
    step: Int = 1,
    random: Random = Random.Default
): BaseRandom<Short>(random) {
    val first: Short
    val last: Short
    val step: Int
    private val base: Short
    private val scalar: RandomShortRange

    init {
        require(step > 0) {
            throw InvalidStep("Invalid step [$step]: must be greater than 0")
        }
        this.base = first
        if (first < last) {
            this.first = first
            this.last = (first .. last step step).last.toShort()
            this.step = step
            this.scalar = RandomShortRange(
                minimum = 0,
                maximum = (floor(((last - first) + step).toDouble()) / step).toInt().toShort(),
                inclusive = false
            )
        } else {
            this.first = last
            this.last = (last .. first step step).last.toShort()
            this.step = (-step)
            this.scalar = RandomShortRange(
                minimum = 0,
                maximum = floor(((first - last) - step).toDouble() / step).absoluteValue.toInt().toShort(),
                inclusive = false
            )
        }
    }

    override fun copyOf(): RandomShortProgression = RandomShortProgression(
        first = this.first,
        last = this.last,
        step = this.step,
        random = this.internalRandom
    )

    override fun nextValue(arguments: Arguments): Short = (this.base + (this.scalar.nextValue() * this.step)).toShort()
}
