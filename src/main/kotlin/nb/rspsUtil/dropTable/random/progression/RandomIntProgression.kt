package nb.rspsUtil.dropTable.random.progression

import nb.util.producerUtil.arguments.Arguments
import nb.rspsUtil.dropTable.random.BaseRandom
import nb.rspsUtil.dropTable.random.range.RandomIntRange
import kotlin.math.absoluteValue
import kotlin.math.floor
import kotlin.random.Random

class RandomIntProgression(
    first: Int,
    last: Int,
    step: Int = 1,
    random: Random = Random.Default
): BaseRandom<Int>(random) {
    val first: Int
    val last: Int
    val step: Int
    private val base: Int
    private val scalar: RandomIntRange

    init {
        require(step > 0) {
            throw InvalidStep("Invalid step [$step]: must be greater than 0")
        }
        this.base = first
        if (first < last) {
            this.first = first
            this.last = (first .. last step step).last
            this.step = step
            this.scalar = RandomIntRange(
                minimum = 0,
                maximum = (floor(((last - first) + step).toDouble()) / step).toInt(),
                inclusive = false
            )
        } else {
            this.first = last
            this.last = (last .. first step step).last
            this.step = (-step)
            this.scalar = RandomIntRange(
                minimum = 0,
                maximum = floor(((first - last) - step).toDouble() / step).absoluteValue.toInt(),
                inclusive = false
            )
        }
    }

    override fun copyOf(): RandomIntProgression = RandomIntProgression(
        first = this.first,
        last = this.last,
        step = this.step,
        random = this.internalRandom
    )

    override fun nextValue(arguments: Arguments): Int = (this.base + (this.scalar.nextValue() * this.step))
}
