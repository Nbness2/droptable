package nb.rspsUtil.dropTable.random

import kotlin.contracts.*

class WeightException(override val message: String) : Exception()

sealed class WeightStatus(val message: String) {
    object ValidWeights :
        WeightStatus("Nothing to see here, commoner... Move along.")
    object InvalidLength :
        WeightStatus("Weights and Items are not the same length.")
    class InvalidWeight(override val offendingIndex: Int) :
        WeightStatus("Weight (at index $offendingIndex) cannot be less than 1"), ContainsOffendingIndex
    class DuplicateWeight(override val offendingIndex: Int, override val secondOffendingIndex: Int) :
        WeightStatus("Cannot contain more than 1 of the same weight (at index $offendingIndex and $secondOffendingIndex)"),
        ContainsOffendingIndex, ContainsSecondOffendingIndex
}

interface ContainsOffendingIndex { val offendingIndex: Int }
interface ContainsSecondOffendingIndex { val secondOffendingIndex: Int }

inline fun <reified T: WeightStatus> WeightStatus.whenStatusIs(block: T.() -> Unit): WeightStatus {
    contract { callsInPlace(block, InvocationKind.AT_MOST_ONCE) }
    if (this is T) block(this)
    return this
}

inline fun WeightStatus.doWhenInvalid(block: WeightStatus.() -> Unit): WeightStatus {
    contract { callsInPlace(block, InvocationKind.AT_MOST_ONCE) }
    if (this !is WeightStatus.ValidWeights) block(this)
    return this
}

inline fun WeightStatus.doWhenValid(block: WeightStatus.() -> Unit): WeightStatus {
    contract { callsInPlace(block, InvocationKind.AT_MOST_ONCE) }
    if (this is WeightStatus.ValidWeights) block(this)
    return this
}

fun WeightStatus.throwWhenBad(): WeightStatus = if (this !is WeightStatus.ValidWeights) {
    throw WeightException(message)
} else {
    this
}