package nb.rspsUtil.dropTable.random

import nb.util.producerUtil.arguments.ArgumentKey
import nb.rspsUtil.dropTable.random.range.RandomLongRange
import kotlin.random.Random

val FLAT_WEIGHT_MODIFIER = ArgumentKey<Double>("flat_weight_modifier")
val PERCENT_WEIGHT_MODIFIER = ArgumentKey<Double>("percent_weight_modifier")
val USE_WEIGHTS = ArgumentKey<IntArray>("use_weights")
val USE_WEIGHT_RANGE = ArgumentKey<RandomLongRange>("use_weight_range")
val PRODUCE_AMOUNT = ArgumentKey<Int>("produce_amount")
val USE_RANDOM = ArgumentKey<Random>("use_random")

private val USE_ITEMS = ArgumentKey<List<*>>("use_items")
fun <T> USE_ITEMS() = USE_ITEMS as ArgumentKey<List<T>>
