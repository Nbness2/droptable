package nb.rspsUtil.dropTable.random

import nb.util.producerUtil.arguments.Arguments
import nb.util.producerUtil.producer.Producer
import kotlin.random.Random

/**
 * [RandomTable] by itself is essentially a [List.random] wrapper. No built in bias.
 *
 * @constructor Creates a [RandomTable] with the given [values]
 * @property [values] The items to initialize the [RandomTable] with
 */
open class RandomTable<T> (items: List<T>, random: Random = Random.Default): BaseRandom<T>(random), Producer<T> {

    val values = items.toList()

    /**
     * Check if [other] is contained in [values]
     *
     * @param [other] The [T] to check is contained in [values]
     * @return [Boolean]
     */
    operator fun contains(other: T): Boolean = other in this.values

    /**
     * Gets a nb.util.producerUtil.random value contained in [values]
     *
     * @param [modifier] unused
     * @return [T]
     */
    override fun nextValue(arguments: Arguments): T = values.random(internalRandom)

    /**
     * Creates a [RandomTable] copy of this
     *
     * @return [RandomTable]
     */
    fun toRandomTable(): RandomTable<T> = RandomTable(values)

    /**
     * Creates a [WeightedTable] copy of this with the given [weights]
     *
     * @param [weights] The weights to use for the [WeightedTable]
     * @return [WeightedTable]<[T]>
     */
    fun toWeightedTable(weights: IntArray): WeightedTable<T> = WeightedTable(values, weights, internalRandom)

    override fun copyOf(): RandomTable<T> = RandomTable(values, internalRandom)

    override fun hashCode(): Int = (47 * 31) + values.hashCode()

    override fun toString(): String = "RandomTable$values"

    override fun equals(other: Any?): Boolean {
        if (other !is RandomTable<*>) return false
        if (other === this) return true
        return this.values == other.values
    }
}
