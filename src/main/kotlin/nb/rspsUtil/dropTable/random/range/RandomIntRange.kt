package nb.rspsUtil.dropTable.random.range

import nb.util.producerUtil.arguments.Arguments
import nb.rspsUtil.dropTable.random.BaseRandom
import kotlin.random.Random

class RandomIntRange(
    minimum: Int,
    maximum: Int,
    private val inclusive: Boolean,
    random: Random = Random.Default
): BaseRandom<Int>(random) {

    val minimum = minOf(minimum, maximum)

    val maximum = maxOf(minimum, maximum)

    override fun copyOf(): RandomIntRange = RandomIntRange(
        minimum = this.minimum,
        maximum = this.maximum,
        inclusive = this.inclusive,
        random = this.internalRandom
    )

    override fun nextValue(arguments: Arguments): Int = this.nextInt(this.minimum, this.maximum, this.inclusive)

    override fun toString(): String = "Int[$minimum -> $maximum${if (inclusive) "]" else ")"}"
}
