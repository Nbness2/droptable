package nb.rspsUtil.dropTable.random.range

import nb.util.producerUtil.arguments.Arguments
import nb.rspsUtil.dropTable.random.BaseRandom
import kotlin.random.Random

class RandomLongRange(
    minimum: Long,
    maximum: Long,
    private val inclusive: Boolean,
    random: Random = Random.Default
): BaseRandom<Long>(random) {

    val minimum = minOf(minimum, maximum)

    val maximum = maxOf(minimum, maximum)

    override fun copyOf(): RandomLongRange = RandomLongRange(
        minimum = this.minimum,
        maximum = this.maximum,
        inclusive = this.inclusive,
        random = this.internalRandom
    )

    override fun nextValue(arguments: Arguments): Long = this.nextLong(this.minimum, this.maximum, this.inclusive)

    override fun toString(): String = "Long[$minimum -> $maximum${if (inclusive) "]" else ")"}"

}
