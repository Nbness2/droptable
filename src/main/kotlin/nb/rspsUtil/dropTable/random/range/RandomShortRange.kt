package nb.rspsUtil.dropTable.random.range

import nb.util.producerUtil.arguments.Arguments
import nb.rspsUtil.dropTable.random.BaseRandom
import kotlin.random.Random

class RandomShortRange(
    minimum: Short,
    maximum: Short,
    private val inclusive: Boolean,
    random: Random = Random.Default
): BaseRandom<Short>(random) {

    val minimum = minOf(minimum, maximum)

    val maximum = maxOf(minimum, maximum)

    override fun copyOf(): RandomShortRange = RandomShortRange(
        minimum = this.minimum,
        maximum = this.maximum,
        inclusive = this.inclusive,
        random = this.internalRandom
    )

    override fun nextValue(arguments: Arguments): Short = this.nextShort(this.minimum, this.maximum, this.inclusive)

    override fun toString(): String = "Short[$minimum -> $maximum${if (inclusive) "]" else ")"}"
}
