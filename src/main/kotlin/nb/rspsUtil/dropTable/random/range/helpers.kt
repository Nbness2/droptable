package nb.rspsUtil.dropTable.random.range

import nb.rspsUtil.dropTable.random.progression.*

infix fun Byte.randTo(other: Byte) = RandomByteRange(this, other, inclusive = true)

infix fun RandomByteRange.step(step: Int) = RandomByteProgression(minimum, maximum, step)

infix fun Short.randTo(other: Short) = RandomShortRange(this, other, inclusive = true)

infix fun RandomShortRange.step(step: Int) = RandomShortProgression(minimum, maximum, step)

infix fun Int.randTo(other: Int) = RandomIntRange(this, other, inclusive = true)

infix fun RandomIntRange.step(step: Int) = RandomIntProgression(minimum, maximum, step)

infix fun Long.randTo(other: Long) = RandomLongRange(this, other, inclusive = true)

infix fun RandomLongRange.step(step: Long) = RandomLongProgression(minimum, maximum, step)
