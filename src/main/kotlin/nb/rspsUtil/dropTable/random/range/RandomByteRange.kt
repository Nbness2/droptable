package nb.rspsUtil.dropTable.random.range

import nb.util.producerUtil.arguments.Arguments
import nb.rspsUtil.dropTable.random.BaseRandom
import kotlin.random.Random

class RandomByteRange(
    minimum: Byte,
    maximum: Byte,
    private val inclusive: Boolean,
    random: Random = Random.Default
): BaseRandom<Byte>(random) {

    val minimum = minOf(minimum, maximum)

    val maximum = maxOf(minimum, maximum)

    override fun copyOf(): RandomByteRange = RandomByteRange(
        minimum = this.minimum,
        maximum = this.maximum,
        inclusive = this.inclusive,
        random = this.internalRandom
    )

    override fun nextValue(arguments: Arguments): Byte = this.nextByte(this.minimum, this.maximum, this.inclusive)

    override fun toString(): String = "Byte[$minimum -> $maximum${if (inclusive) "]" else ")"}"
}
