package nb.rspsUtil.dropTable

import nb.rspsUtil.dropTable.defaultKeys.Keys
import nb.rspsUtil.dropTable.exception.NoTargetFound
import nb.util.producerUtil.arguments.Arguments
import nb.util.producerUtil.producer.*
import nb.rspsUtil.dropTable.random.FLAT_WEIGHT_MODIFIER
import nb.rspsUtil.dropTable.random.PERCENT_WEIGHT_MODIFIER
import kotlin.random.Random

open class MultiRollTable<T: Any, R: Any>(
    override val tableName: String,
    chancesAndItems: List<ProducerChancePair<R>>,
    val random: Random = Random.Default,
    override inline val onSelect: OnSelectContext<T, List<R>>.() -> Unit = { }
): Table<T, List<R>> {

    companion object {
        val NO_TABLE = MultiRollTable<Any, Any>("NO_TABLE_PROVIDED", listOf())
    }

    val chancesAndItems = chancesAndItems.sortedBy(ProducerChancePair<R>::chance)

    override fun produceRaw(arguments: Arguments): List<R> {

        val pickedValues = mutableListOf<R>()

        val rollsPerItem = arguments.getOrDefault(Keys.ROLLS_PER_ITEM, 1)

        chancesAndItems.forEach { pcp ->

            repeat(rollsPerItem) {

                val rollResult = pcp.chance.roll(
                    flatModifier = arguments[FLAT_WEIGHT_MODIFIER, 0.0],
                    percentModifier = arguments[PERCENT_WEIGHT_MODIFIER, 0.0].toPercent(),
                    random = this.random
                )
                if (rollResult) {
                    val produceResults = pcp.value.produce(arguments)
                    pickedValues.add(produceResults.valueProduced)
                }
            }
        }
        return pickedValues
    }

    override fun produce(arguments: Arguments): ProducerResult<List<R>> {

        val producedValues: MutableList<R> = mutableListOf()

        producedValues.addAll(produceRaw(arguments))

        val target = arguments[Keys.USE_TARGET<T>()] ?: throw NoTargetFound(this)

        val selectContext = OnSelectContext(
            target = target,
            mainName = this.tableName,
            subName = this.tableName,
            extendedSubName = this.tableName,
            valueProduced = producedValues as List<R>,
            argumentsUsed = arguments
        )

        selectContext.onSelect()

        return ProducerResultImpl(producedValues, arguments)
    }

    open class Builder<T: Any, R: Any>(
        tableName: String = "NO_TABLE_NAME",
        @TableDSL inline var onSelect: OnSelectContext<T, List<R>>.() -> Unit = { }
    ) {

        private val producerChancePairs: MutableList<ProducerChancePair<R>> = mutableListOf()

        @TableDSL
        var random: Random = Random.Default

        @TableDSL
        fun random(random: Random): Builder<T, R> {
            this.random = random
            return this
        }

        @TableDSL
        var name: String = tableName

        @TableDSL
        fun name(newName: String): Builder<T, R> {
            this.name = newName
            return this
        }

        @TableDSL
        fun onSelect(block: OnSelectContext<T, List<R>>.() -> Unit): Builder<T, R> {
            this.onSelect = block
            return this
        }

        @TableDSL
        fun addProducer(chance: Chance, producer: Producer<R>): Builder<T, R> {
            producerChancePairs.add(ProducerChancePair(producer, chance))
            return this
        }

        @TableDSL
        fun addProducer(chance: Double, producer: Producer<R>) = addProducer(chance.toChance(), producer)

        @TableDSL
        infix fun Producer<R>.chance(pickChance: Chance): Builder<T, R> = addProducer(pickChance, this)

        @TableDSL
        infix fun Chance.chance(producer: Producer<R>): Builder<T, R> = addProducer(this, producer)

        @TableDSL
        infix fun Producer<R>.chance(pickChance: Double): Builder<T, R> = addProducer(pickChance, this)

        @TableDSL
        infix fun Double.chance(producer: Producer<R>): Builder<T, R> = addProducer(this, producer)


        fun build(): MultiRollTable<T, R> = MultiRollTable(
            tableName = name,
            chancesAndItems = producerChancePairs,
            random = random,
            onSelect = onSelect
        )
    }
}
