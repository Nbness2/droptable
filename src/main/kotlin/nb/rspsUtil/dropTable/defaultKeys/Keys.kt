package nb.rspsUtil.dropTable.defaultKeys

import nb.util.producerUtil.arguments.ArgumentKey
import nb.rspsUtil.dropTable.ExclusiveTable

object Keys {
    @JvmStatic val MAIN_NAME = ArgumentKey<String>("main_name")

    @JvmStatic val USE_TABLE = ArgumentKey<ExclusiveTable<*, *>>("use_table")

    @JvmStatic val SUB_NAME = ArgumentKey<String>("sub_name")

    @JvmStatic val EXTENDED_SUB_NAME = ArgumentKey<String>("extended_sub_name")

    @JvmStatic val ROLLS_PER_ITEM = ArgumentKey<Int>("rolls_per_item")

    @JvmStatic fun <T : Any> USE_TARGET() = USE_TARGET as ArgumentKey<T>

    @JvmStatic val USE_TARGET = ArgumentKey<Any>("use_target")
}
