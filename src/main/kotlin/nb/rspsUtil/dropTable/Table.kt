package nb.rspsUtil.dropTable

import nb.util.producerUtil.producer.Producer

interface Table<T: Any, R: Any>: Producer<R> {
    val tableName: String

    val onSelect: OnSelectContext<T, R>.() -> Unit
}