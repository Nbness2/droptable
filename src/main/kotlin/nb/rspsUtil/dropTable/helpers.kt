package nb.rspsUtil.dropTable

import nb.util.producerUtil.arguments.Arguments
import nb.util.producerUtil.producer.Producer
import kotlin.random.Random

fun <T: Any, R: Any> buildExclusiveTable(
    tableName: String = "NO_TABLE_NAME",
    block: ExclusiveTable.Builder<T, R>.() -> Unit
): Table<T, R> {
    val builder = ExclusiveTable.Builder<T, R>(tableName = tableName)
    builder.apply(block)
    return builder.build()
}

fun <T: Any, R: Any> buildMultiRollTable(
    tableName: String = "NO_TABLE_NAME",
    block: MultiRollTable.Builder<T, R>.() -> Unit
): MultiRollTable<T, R> = MultiRollTable.Builder<T, R>(tableName = tableName)
    .apply(block)
    .build()

class OnSelectContext<T: Any, R: Any>(
    val target: T,
    val mainName: String,
    val subName: String,
    val extendedSubName: String,
    val valueProduced: R,
    val argumentsUsed: Arguments
)

@DslMarker annotation class TableDSL

@JvmInline
value class Percent(val percent: Double): Comparable<Percent> {

    companion object {
        val ZERO_PERCENT = Percent(0.0)
        val ONE_HUNDRED_PERCENT = Percent(100.0)
    }

    init {
        require(!percent.isNaN()) { "Percent cannot be NaN" }
        require(!percent.isInfinite()) { "Percent cannot be infinite " }
    }

    override operator fun compareTo(other: Percent): Int = this.percent.compareTo(other.percent)
    operator fun compareTo(other: Double): Int = this.percent.compareTo(other)
}

@JvmInline
value class Chance(val chance: Percent): Comparable<Chance> {
    init {
        require(chance > 0.0) { "Chance must be greater than 0.0%" }
        require(chance <= 100.0) { "Chance must be less than or equal to 100.0%" }
    }

    fun roll(
        flatModifier: Double = 0.0,
        percentModifier: Percent = 0.0.toPercent(),
        random: Random = Random.Default
    ): Boolean {
        val initialChance = chance.percent
        val flatModifiedChance = initialChance + flatModifier
        val percentModifiedChance = flatModifiedChance * (1 + (percentModifier.percent / 100))
        val randomValue = random.nextDouble(from = 0.0, until = 100.0 + Double.MIN_VALUE)
        return percentModifiedChance >= randomValue
    }

    override operator fun compareTo(other: Chance): Int = this.chance.compareTo(other.chance)
    operator fun compareTo(other: Double): Int = this.chance.compareTo(other)

    companion object {
        operator fun invoke(chance: Double) = Chance(chance.toPercent())
    }
}

fun Double.toPercent(): Percent = Percent(this)

fun Double.toChance(): Chance = Chance(this)

data class ProducerChancePair<T: Any>(val value: Producer<T>, val chance: Chance)

infix fun <T: Any> Producer<T>.chance(chance: Chance) = ProducerChancePair(this, chance)

infix fun <T: Any> Producer<T>.chance(chance: Double) = ProducerChancePair(this, chance.toChance())
