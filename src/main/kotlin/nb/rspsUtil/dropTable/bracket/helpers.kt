package nb.rspsUtil.dropTable.bracket

import nb.util.producerUtil.arguments.Arguments
import nb.util.producerUtil.producer.Producer

class ProducerResultContext<R: Any>(
    val producedValue: R,
    val argumentsUsed: Arguments
)

class TargetConditionContext<T: Any>(
    val target: T,
    val argumentsUsed: Arguments
)

class ProducerImplContext<R: Any>(
    val producerList: List<Producer<R>>,
    val arguments: Arguments
)

